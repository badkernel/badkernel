﻿// See https://aka.ms/new-console-template for more information
using System;
//L'instruction suivante demande a l'utilisateur d'entrer un message
//Console.WriteLine("Entrer un message");

//L'instruction demande a l'utilisateur d'entrer un message dans la console et la stock dans la variable line
//var line  = Console.ReadLine();
//Console.WriteLine(line);
//Declaration de variable de type entier
//byte ff = 3; // val max 255 val min -255
//short ShEx = 32010; //val max 32767 val min -32767
//int InEx = 1233333333;/* S'en tenir a Int pour les plus grande valeur dans le but de mininimiser. -2147483648 a 2147483647;
//l espace memoire. A moins d'une exception il devrait suffire. */
//long ExLong = 111111222222; //Type qui contient les valeur les plus haute et prend le plus d espace memoire

//Console.WriteLine(ff+ShEx+InEx);
//Variable avec virgule
//float FlEx;// pas oublier de mettre un f a la fin !@#$
//FlEx = 1.233333f;
//double ExDouble = 1.3333d;// Permais un chiffre plus grand que float...Le d a la fin dans le cas de double n<est pas obligatoire
//decimal ExDec = 1.34m; // pas oublier de mettre un m a la fin Type le plus grand de chiffre avec virgule


//variable bool (true or false)
//bool ExBool = true;


//string HelloWord = "Benn euuu Hello World";// pour chaine de caractere
//char L_A = 'A'; //stock un caractere




//Console.WriteLine(ff);

//int entier; //declaration
//int entier01 = 2; //declaration avec initialisation / assignation

//entier = 2; //afectation : j ai affecte une valeur a ma variable
//Console.WriteLine(entier);
//entier = 1 + 2 * 5;//affectation d une expression mathematique

//Console.WriteLine(entier);

//copie d<une varaible a un autre

//entier = entier01;//affectation

//Console.WriteLine(entier);

//int a =3;
//a += 5;
//Console.WriteLine(a);

//String nomDuMembre;// Declaration variable
//float kilogrammes;// Declaration
//Console.WriteLine("Quel est le nom du membre");
//nomDuMembre = Console.ReadLine();// assigne la valeur entrer au clavier a la variable nomDuMembre
//Console.WriteLine("quel est le poids de " + nomDuMembre + " ?");// ecrit
//String poids = Console.ReadLine();//affecte la valeur au clavier al a variable string pods
//kilogrammes = float.Parse(poids);// Cast la valeur string en valeur float avec Parse
//type de conversation. Parse(valeur/varable a convertir
//float.TryParse(Console.ReadLine(), out kilogrammes);
//nsole.WriteLine("Bienvenue a " + nomDuMembre + " pesant " + kilogrammes+"Kg");// ecrit le resultat 

//conversion explicit
//float reel01 = 52.5f;
//int entier05;
//entier05 =(int) reel01;

//conversion implicite
//reel01 = entier05;

//permutation de variable a l aide d un variable temporaire
//int tmp;
//int a = 2;
//int b = 8;
//tmp = a;
//a = b;
//b = tmp;
//console.WriteLine("Entrer la valeur de A");
//int A = int.Parse(Console.ReadLine());
//Console.WriteLine("Entrer la valeur de B");
//int B = int.Parse(Console.ReadLine());

//int tmp = A;
//A = B;
//B = tmp;
//Console.WriteLine("La valeur de A est egal " + A +" et  la valeur de B est egal a " + B);

//double a, b;
//a = (10 / 3f+3)/ 5;
//b = ((double) 10 / 3 + 3) / 5;
//decimal c;
//c = 10.99m + 11.11m;
//int d = (int)10f * 2;

//Console.WriteLine(a);
//Console.WriteLine(b);
//Console.WriteLine(c);
//Console.WriteLine(d);


//permutation de variable a l aide d un variable temporaire
//int tmp;
//int x = 5;
//int y = 8;
//tmp = x;
//x = y;
//y = tmp;

//Console.WriteLine(x);
//Console.WriteLine(y);

/*int tmp;// declaration valeur tmp
int x = 2;// declaration et affectation de x
int y = 8;// declaration et affectation de y
tmp = x;// on assigne la valeur de x a tmp
x = y;//on assighe la valeur de y a x
y = tmp;//on assigne la valeur de tmp(contient la valeur initial de x)

Console.WriteLine("la valeur de X est " + x);//verification sur la console
Console.WriteLine("la valeur de Y est " + y);//verfication sur la console*/

/*int tmp, tmp2, tmp3;//declaration des variables tmp, tmp1 et tmp2

int a = 5;// initialisation est affectation de la valeur 5 a la variable a
int b = 10;//nitialisation et affection de la valeur 8 a la variable 
int c = 2;//initialisation et affection de la valeur 2 a la variable c
tmp = a;//affectation de la valeur de a la variable tmp
tmp2 = b;//affectation de la valeur de b la variable tmp2
tmp3 = c;//affectation de la valeur de c la variable tmp3
b = tmp;//affection de la valeur de la variable tmp a la variable b
c = tmp2;//affection de la valeur de la variable tmp a la variable b
a = tmp3;//affection de la valeur de la variable tmp a la variable b

//verification de la valeur des variable a, b et c a la fin
Console.WriteLine("la valeur de a est " + a);
Console.WriteLine("la valeur de b est " + b);
Console.WriteLine("la valeur de c est " + c);
*/



//1.11
/*double a;
a =(10/3f+3)/5d;
double b;
b = (10/3+3)/5;
decimal c;
c = 10.99m+11.11m;
float d = (int) 10f*2;
Console.WriteLine("resultat a = " +a);//resultat a = 1.26666
Console.WriteLine("resultat b = " +b);//resultat b = 1
Console.WriteLine("resultat c = " +c);//resultat c = 22.10
Console.WriteLine("resultat c = " +d);//resultat d = 20

//j'ai changé le "int d" pour "float d" et remis la demande explicite int*/


//Condition a l'aide de switch
//
// Console.WriteLine("Entrer un numéro de mois [1 à 12]");
// byte mois = byte.Parse(Console.ReadLine());//declare et initialise une variable byte qui prend la valeur entrer dans la console(string) et la convertit en byte pour la stocker dans la variabler mois
//
//
//
// switch (mois)//execute l'instruction selon la valeur de la variable mois
// {
//     case 1:
//     case 3:
//     case 5:
//     case 7:
//     case 8:
//     case 10:
//     case 12:
//         Console.WriteLine("Le mois #"+ mois+" contient 31 jours");//execute si la variable mois est egale a 1
//         break;
//         
//     case 2://execute le code suivant si la variable mois est egale a 2
//         Console.WriteLine("Entrer l'année en cours avec un format de 4 chiffres");//demande d'entrer l'anne car le mois de février n'a pas le nombre de jours s'Il est bissextile ou non
//         short annee = short.Parse(Console.ReadLine());// declaration et affection de la valeur anne qui est egale a l'entre de la console(donc string) convertit en int.
//         
//         if(annee % 4 == 0) { // verifie si l'annee est bissextile a l'aide du modulo
//             Console.WriteLine("Le mois #"+ mois+" contient 29 jours et "+ annee + " est un annee bissextile");
//         }
//         else { //si la condition du if est faux les instructions suivante seront executer
//         
//          Console.WriteLine("Le mois #"+ mois+" contient 28 jours et " + annee + " n'est pas bissextile");
//         }
//         break;
//     
//     case 4://execute si la variable mois est egale a 4
//     case 6://execute si la variable mois est egale a 6
//     case 9://execute si la variable mois est egale a 9
//     case 11://execute si la variable mois est egale a 11
//         Console.WriteLine("Le mois #"+ mois+" contient 30 jours");
//         break;
//     
//     default:// s'execute si la valeur de mois n'est pas egal a un chiffre entre 1 et 12
//         Console.WriteLine("Le mois "+ mois+" n'est pas un mois valide ");
//         break;
//     
// }


//2.1

// int nbr = int.Parse(Console.ReadLine());
// Console.WriteLine("Entrer un nombre");
// Console.WriteLine("Le carre de "+ nbr +" est égale a " + nbr * nbr);


// 2.2
//  Console.WriteLine("Quel est le prix de vente");
//
// double prixDeVente = double.Parse(Console.ReadLine());
//
// double tps = Math.Round((prixDeVente * 0.05), 2 );
//  
//  double tvq = Math.Round((prixDeVente) * 0.09975, 2);
//  double prixTotal = Math.Round(prixDeVente + tps + tvq, 2);
//
//  Console.WriteLine("Montant : " + prixDeVente + "$");
//  Console.WriteLine("TPS : "+ tps + "$");
//  Console.WriteLine("TVQ : "+ tvq + "$");
//  Console.WriteLine("Total : "+ prixTotal + "$");
//


//2.3
// Console.WriteLine("Entrez la température de l'eau en celsuis");
// float T_eau = float.Parse(Console.ReadLine());
//
// if (T_eau > 0 && T_eau < 100)
// {
//     Console.WriteLine("L'eau est dans un état liquide");
// }
//     
// else if (T_eau >= 100)
// {
//     Console.WriteLine("L'eau est dans un état gazeux");
// }
//
// else
// {
//     Console.WriteLine("l'eau est a l'état solide");
// }


//2.4
// Console.WriteLine("Entrez le diametre de la sphere");
// double d_sphere = double.Parse(Console.ReadLine());
// double diameter = d_sphere;
// double rayon = diameter / 2;
// double v_Sphere = (((4 * Math.PI) * Math.Pow(rayon, 3)) / 3);
//
// Console.WriteLine(Math.Pow(rayon, 3));
//
// Console.WriteLine("Le volume du cercle est de "+ v_Sphere+ " cm3");



//2.5

// Console.WriteLine("Entrez un nombre : ");
// int nbr = int.Parse(Console.ReadLine());
//
// if (nbr % 5 == 0 && nbr % 7 != 0)
// {
//     Console.WriteLine("le nombre "+ nbr+ " est valide");
// }
//
// else
// {
//     Console.WriteLine("le nombre "+ nbr+ " est invalide ");
// }


//2.6

//  Console.WriteLine("Entrez une année :");
// short annee = short.Parse(Console.ReadLine());
// if (annee % 400 == 0)
// {
//     Console.WriteLine(annee+" est une année bissextile");
// }
// else
// {
//     if (annee % 4 == 0 && annee % 100 != 0)
//     {
//         Console.WriteLine(annee + " est une année bissextile");
//
//     }
//     else
//     {
//         Console.WriteLine(annee + " n'est pas bissextile");
//     }
// }
//    

//2.7

// Console.WriteLine("Quel numéro de jour somme nous ");
// byte N_jour = byte.Parse(Console.ReadLine());
//
// switch (N_jour)
// {
//    
//     case 1:
//         Console.WriteLine("Bon Dimanche a toi");
//         break;
//     case 2:
//         Console.WriteLine("Bon Lundi a toi");
//         break;
//
//     case 3:
//         Console.WriteLine("Bon Mardi a toi");
//         break;
//
//     case 4:
//         Console.WriteLine("Bon Mercredi a toi");
//         break;
//
//     case 5:
//         Console.WriteLine("Bon Jeudi a toi");
//         break;
//
//     case 6:
//         Console.WriteLine("Bon Vendredi a toi");
//         break;
//
//     case 7:
//         Console.WriteLine("Bon Samedi a toi");
//         break;
//
//     default:
//         Console.WriteLine("Tu es perdu dans l’espace toi!");
//         break;
//     
//
// }